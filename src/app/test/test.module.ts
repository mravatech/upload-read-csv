import { NgModule }                         from '@angular/core';
import { CommonModule }                     from '@angular/common';
import { Routes, RouterModule }             from '@angular/router';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpModule }                       from '@angular/http';
import { TestComponent }                    from './test.component';
import { TestRoutes }                       from './test.routes';
import { FileUtil }                         from './file.util';
import { Constants }                        from './test.constants';

@NgModule({

    imports: [ 
        CommonModule,
        TestRoutes,
        FormsModule,
        RouterModule,
        ReactiveFormsModule,
        HttpModule
    ],

    declarations:[
        TestComponent
    ],

    providers: [
        FileUtil,
        Constants
    ],
})

export class TestModule{}